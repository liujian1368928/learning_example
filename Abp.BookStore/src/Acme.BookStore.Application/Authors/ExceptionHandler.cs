﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.ExceptionHandling;

namespace Acme.BookStore.Authors
{
    public class ExceptionHandler : ExceptionSubscriber
    {
        public override async Task HandleAsync(ExceptionNotificationContext context)
        {
            context.Exception.ToString(); 
        }
    }
}
