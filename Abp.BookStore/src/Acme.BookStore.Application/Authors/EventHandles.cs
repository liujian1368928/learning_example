﻿using Acme.BookStore.Authors;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Domain.Entities.Events.Distributed;
using Volo.Abp.EventBus;
using Volo.Abp.EventBus.Distributed;

namespace Acme.BookStore.Authors
{
    public class EventHandles : ILocalEventHandler<AuthorCreatedEvent>, ITransientDependency
    {
        public async Task HandleEventAsync(AuthorCreatedEvent eventData)
        {
            eventData.Author.Name.ToString();
        }


    }
}
