﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DDD.Domain.Events
{
    public class UserCreatedEvent:IEvent
    {
        public int ID { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }
    }
}
