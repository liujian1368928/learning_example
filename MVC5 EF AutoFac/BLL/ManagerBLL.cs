﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace BLL
{
    public class ManagerBLL : IBLL.IManagerBLL
    {
        IDAL.IManagerDAL dal = null;
        public ManagerBLL(IDAL.IManagerDAL dal)
        {
            this.dal = dal;
        }

        public int Add(ManagerModel model)
        {
            return dal.Add(model);
        }

        public int Update(ManagerModel model)
        {
            return dal.Update(model);
        }

        public ManagerModel GetModel(string UserName)
        {
           return dal.GetModel(UserName);
        }

        public int Delete(string UserName)
        {
            return dal.Delete(UserName);
        }
    }
}
