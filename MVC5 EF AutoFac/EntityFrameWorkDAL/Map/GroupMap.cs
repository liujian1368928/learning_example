﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFrameWorkDAL.Map
{
    public class GroupMap : EntityTypeConfiguration<Model.GroupModel>
    {
        public GroupMap()
        {
            this.ToTable("Group");
            this.HasKey(e => e.Code);
            this.Property(e => e.GroupName).HasMaxLength(32).IsUnicode(true);
            this.HasMany(e => e.Manages);
        }
    }
}
