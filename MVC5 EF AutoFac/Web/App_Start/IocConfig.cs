﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Microsoft.Extensions.Configuration;
using System.Reflection;

namespace Web
{
    public class IocConfig
    {
        public static void RegisterIoc()
        {
            var builder = new ContainerBuilder();

            builder.RegisterControllers(typeof(IocConfig).Assembly);
            var bll = Assembly.Load("BLL");
            builder.RegisterAssemblyTypes(bll).AsImplementedInterfaces();

           
            var dal =  Assembly.Load("EntityFrameWorkDAL");
            builder.RegisterAssemblyTypes(dal).AsImplementedInterfaces();

            var container = builder.Build();
            var resolver = new AutofacDependencyResolver(container);
            DependencyResolver.SetResolver(resolver);
        }
    }
}