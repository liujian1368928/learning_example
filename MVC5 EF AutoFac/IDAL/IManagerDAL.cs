﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDAL
{
    public interface IManagerDAL
    {
         Model.ManagerModel GetModel(string UserName);

         int Add(Model.ManagerModel model);

         int Add(IEnumerable<Model.ManagerModel> models);

         int Update(Model.ManagerModel model);

         int Update(IEnumerable<Model.ManagerModel> models);

         int Delete(string UserName);
    }
}
