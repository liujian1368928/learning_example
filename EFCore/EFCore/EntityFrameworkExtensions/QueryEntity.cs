﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EFCore.EntityFrameworkExtensions
{
    /// <summary>
    /// 查询实体
    /// </summary>
    public class QueryEntity
    {
        /// <summary>
        /// 字段名称
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// 值
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// 操作方法，对应OperatorEnum枚举类
        /// </summary>
        public OperatorEnum Operator { get; set; }

        /// <summary>
        /// 逻辑运算符，只支持AND、OR
        /// </summary>
        public string LogicalOperator { get; set; }

        /// <summary>
        /// 子查询条件
        /// </summary>
        public IList<QueryEntity> ChildQueryEntitys { get; set; }
    }
}
