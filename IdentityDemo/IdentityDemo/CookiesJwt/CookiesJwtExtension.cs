﻿using IdentityDemo.CookiesJwt;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class CookiesJwtExtension
    {
        public static CookieAuthenticationOptions UseJwt(this CookieAuthenticationOptions options)
        {
            options.TicketDataFormat = new TicketJsonDataFormat<AuthenticationTicket>(TicketJsonSerializer.Default, new DataJwtProtector());
            return options;
        }
    }
}
