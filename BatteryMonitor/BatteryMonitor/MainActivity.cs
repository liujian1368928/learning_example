﻿using System;
using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Views;
using AndroidX.AppCompat.Widget;
using AndroidX.AppCompat.App;
using Google.Android.Material.FloatingActionButton;
using Google.Android.Material.Snackbar;

namespace BatteryMonitor
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        protected AppCompatTextView lbl1;
        protected AppCompatTextView lbl2;
        protected System.Timers.Timer timer;
        protected BatteryManager manager;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            if (savedInstanceState == null)
            {
                base.OnCreate(savedInstanceState);
                Xamarin.Essentials.Platform.Init(this, savedInstanceState);
                SetContentView(Resource.Layout.activity_main);

                Toolbar toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
                SetSupportActionBar(toolbar);

                FloatingActionButton fab = FindViewById<FloatingActionButton>(Resource.Id.fab);
                fab.Click += FabOnClick;


                lbl1 = FindViewById<AppCompatTextView>(Resource.Id.lbl1);
                lbl2 = FindViewById<AppCompatTextView>(Resource.Id.lbl2);
                manager = (BatteryManager)GetSystemService(BatteryService);

                lbl2.Text = savedInstanceState?.GetString("lbl2", "");
                lbl2.Text += "=>OnCreate";


            }
        }

        
        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            var str = "";
            foreach (var i in Enum.GetValues(typeof(BatteryStatus))) {
                str += $"BatteryStatus.{Enum.GetName(typeof(BatteryStatus), i)}:{manager.GetIntProperty(i.GetHashCode())}\r\n";
            }
            foreach (var i in Enum.GetValues(typeof(BatteryProperty)))
            {
                str += $"BatteryProperty.{Enum.GetName(typeof(BatteryProperty), i)}:{manager.GetIntProperty(i.GetHashCode())}\r\n";
            }
            foreach (var i in Enum.GetValues(typeof(BatteryHealth)))
            {
                str += $"BatteryHealth.{Enum.GetName(typeof(BatteryHealth), i)}:{manager.GetIntProperty(i.GetHashCode())}\r\n";
            }
            foreach (var i in Enum.GetValues(typeof(BatteryPlugged)))
            {
                str += $"BatteryPlugged.{Enum.GetName(typeof(BatteryPlugged), i)}:{manager.GetIntProperty(i.GetHashCode())}\r\n";
            }
            str += $"IsCharging:{manager.IsCharging}\r\n";
            str += $"ExtraVoltage:{manager.GetIntProperty(BatteryManager.ExtraVoltage.GetHashCode())}\r\n";
            str += $"ExtraTechnology:{manager.GetIntProperty(BatteryManager.ExtraTechnology.GetHashCode())}\r\n";
            str += $"ExtraTemperature:{manager.GetIntProperty(BatteryManager.ExtraTemperature.GetHashCode())}\r\n";
            str += $"ExtraPresent:{manager.GetIntProperty(BatteryManager.ExtraPresent.GetHashCode())}\r\n";
            str += $"ExtraScale:{manager.GetIntProperty(BatteryManager.ExtraScale.GetHashCode())}\r\n";
            str += $"ExtraPlugged:{manager.GetIntProperty(BatteryManager.ExtraPlugged.GetHashCode())}\r\n";
            str += $"ExtraLevel:{manager.GetIntProperty(BatteryManager.ExtraLevel.GetHashCode())}\r\n";
            str += $"ExtraHealth:{manager.GetIntProperty(BatteryManager.ExtraHealth.GetHashCode())}\r\n";
            
            lbl1.Text = str;
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_main, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            if (id == Resource.Id.action_settings)
            {
                return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        private void FabOnClick(object sender, EventArgs eventArgs)
        {
            View view = (View) sender;
            Snackbar.Make(view, "Replace with your own action", Snackbar.LengthLong)
                .SetAction("Action", (View.IOnClickListener)null).Show();
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        protected override void OnStart()
        {
            base.OnStart();
            lbl2.Text +=  DateTime.Now.ToString("HH:mm:ss") +"=>OnStart";
        }
        protected override void OnRestart()
        {
            base.OnRestart();
            lbl2.Text +=  DateTime.Now.ToString("HH:mm:ss") + "=>OnRestart";
        }
        protected override void OnPause()
        {
            base.OnPause();
            lbl2.Text +=  DateTime.Now.ToString("HH:mm:ss") + "=>OnPause";
            timer.Stop();
        }
        protected override void OnStop()
        {
            base.OnStop();
            lbl2.Text +=  DateTime.Now.ToString("HH:mm:ss") + "=>OnStop";


        }

        protected override void OnResume()
        {
            base.OnResume();
            lbl2.Text +=  DateTime.Now.ToString("HH:mm:ss") + "=>OnResume";
            if (timer == null)
            {
                timer = new System.Timers.Timer(1000);
                timer.Elapsed += Timer_Elapsed;
            }
            Timer_Elapsed(null, null);
            timer.Start();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            lbl2.Text +=  DateTime.Now.ToString("HH:mm:ss") + "=>OnDestroy";
        }

        public override void OnBackPressed()
        {
         
            lbl2.Text +=  DateTime.Now.ToString("HH:mm:ss") + "=>OnBackPressed";

            MoveTaskToBack(true);
           // base.OnBackPressed();
        }

        protected override void OnSaveInstanceState(Bundle outState)
        {
            outState.PutString("lbl2", lbl2.Text);
            base.OnSaveInstanceState(outState);
        }

        protected override void OnRestoreInstanceState(Bundle savedInstanceState)
        {
            lbl2.Text = savedInstanceState?.GetString("lbl2", "");
            lbl2.Text +=  DateTime.Now.ToString("HH:mm:ss") + "=>OnRestoreInstanceState";
            base.OnRestoreInstanceState(savedInstanceState);
        }

        protected override void OnPostResume()
        {
            base.OnPostResume();
            lbl2.Text += DateTime.Now.ToString("HH:mm:ss") + "=>OnPostResume";
        }
    }
}
