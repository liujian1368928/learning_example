﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Utils.APIClient;

namespace Utils.APIClient.Example
{
    public class MyAPIClient:APIClient
    {
        public MyAPIClient(string serverUrl) : base(serverUrl)
        {
        }

        protected override BaseResponse<T> OnError<T>(Exception e, BaseReqeust<T> req, string url)
        {
            return base.OnError(e, req, url);
        }

        protected override object GetReqeustBody<T>(BaseReqeust<T> req, WebRequest client)
        {
            return base.GetReqeustBody(req, client);
        }

        protected override T GetResponse<T>(string rspStr, HttpWebResponse rsp, BaseReqeust<T> req, object requestBody, IDictionary<string, FileItem> files, WebRequest client)
        {
            return base.GetResponse(rspStr, rsp, req, requestBody, files, client);
        }

        protected override Stream GetSaveFileStream<T>(BaseReqeust<T> req, WebRequest client)
        {
            return base.GetSaveFileStream(req, client);
        }
    }
}
