﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Utils.APIClient
{
    public class RemoteServiceAttribute :Attribute
    {
        /// <summary>
        /// Url
        /// </summary>
        public string Url { get; set; }

        public string Method { get; set; }


        public RemoteServiceAttribute(string Url,string Method="POST")
        {
            this.Url = Url;
            this.Method = Method;
        }
    }
}