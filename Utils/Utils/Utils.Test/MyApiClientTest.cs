using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Specialized;
using Utils.APIClient.Example;
using Utils.APIClient.Example.APIModel;

namespace Utils.APIClient.Test
{
    [TestClass]
    public class MyApiClientTest
    {
        [TestMethod]
        public void BaiduSearchTest()
        {
            string serverUrl = "https://www.baidu.com/";
            var client = new MyAPIClient(serverUrl);
            var rsp=client.Execute(new BaiduRequest()
            {
                wd = "�й�"
            });
            Assert.AreEqual(rsp.HttpWebResponse.StatusCode, System.Net.HttpStatusCode.OK);

            var imgRsp = client.Execute(new BaiduLogRequest()
            {
                img = "PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png"
            });
            Assert.AreEqual(imgRsp.HttpWebResponse.StatusCode, System.Net.HttpStatusCode.OK);
            Assert.AreEqual(imgRsp.HttpWebResponse.ContentType, "image/png");
            Assert.AreEqual(imgRsp.Data.FileName, "PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png");
        }

        [TestMethod]
        public void GetIPTest()
        {
            var serverUrl = "https://apidatav2.chinaz.com/";
            var client = new MyAPIClient(serverUrl);
            var rsp=client.Execute(new GetIPRequest()
            {
                 key="test",
                 ip= "14.215.177.38"
            });
            Assert.AreEqual(rsp.HttpWebResponse.StatusCode, System.Net.HttpStatusCode.OK);
            Assert.AreEqual(rsp.Data.StateCode, 0);
            Assert.AreEqual(rsp.Data.Reason, "key����");
        }

    }
}
