﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IOPerformanceTest
{
    public class Performance
    {
        public long ElapsedSeconds { get; set; }

        public long WriteByteCount { get; set; }
    }

 
}
