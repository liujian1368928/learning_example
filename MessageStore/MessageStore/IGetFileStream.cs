﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DataStore
{
    public interface IGetFileStream
    {
        IEnumerable<Stream> GetStreams();

        Stream NewStream();

    }
}
