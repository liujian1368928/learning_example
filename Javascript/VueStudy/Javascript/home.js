var app=new Vue({
   el:'#contains',
   data:{
      select_val:'',
      select_oldval:'',
      Items:[
         {Value:'1',Text:'测试1'},
         {Value:'2',Text:'测试2'},
         {Value:'3',Text:'测试3'},
         {Value:'4',Text:'测试4'},
         {Value:'5',Text:'测试5'},
         {Value:'6',Text:'测试6'}
     ],
     componentName:'component1'
   },
   methods:{
      SelectChage:function(item,oldItem){
          this.select_val=item==null?'':item.Text;
          this.select_oldval=oldItem==null?'':oldItem.Text;
      }
   },
   components:{
      'component1':{ template:'<div>这是组件1</div>'},
      'component2':{ template:'<div>这是组件2</div>'},
      'v-h':{
         props:{
            level:Number,
            required:true,
         },
         render:function(createElment){
            var el=createElment('h' + this.level,this.$slots.default);
            return el;

         }
      }
   }
});