const webpack = require('webpack');
const path=require('path');
const toml = require('toml');
const yaml = require('yamljs');
const json5 = require('json5');
const HtmlWebpackPlugin = require('html-webpack-plugin');
//const { CleanWebpackPlugin }= require('clean-webpack-plugin');
const { WebpackManifestPlugin } = require('webpack-manifest-plugin');

module.exports={
    entry:{
        index:['./src/index.js'],
        print:'./src/print.js'
    },
    output:{
        path:path.resolve(__dirname,'dist'),
        filename:'[name].[chunkhash].js',
        //publicPath: '/'   //webpacek-dev-server 虚拟目录，设置浏览器访问的根地址
        clean:true    //发布时清理dist
    },
    devServer: {                    //webpack server 服务器
        static: {
           directory: path.join(__dirname, 'dist'),
        },
        port: 9010,
        compress:true
    },
    plugins:[
        //new CleanWebpackPlugin(),  //发布时清理dist
        new HtmlWebpackPlugin({    //自动生成index.html
            title:'webpack学习'
        }),
        new WebpackManifestPlugin({})    //生成manifest.json映射图
    ],
    module:{
        rules:[
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                  loader: 'babel-loader',
                  options: {
                    presets: ['@babel/preset-env']
                  }
                }
            },
            {
                test: /\.css$/i,
                include: path.resolve(__dirname, 'src'),  //仅解析src目录下的文件
                use: ['style-loader','css-loader']
            },
            {
                test: /\.s[ac]ss$/i,
                use:['style-loader','css-loader','sass-loader']
            },
            {
                test: /\.(png|svg|jpg|jpeg|gif)$/i,
                type: 'asset/resource',
                generator: {
                    filename: 'images/[hash][ext][query]'
                }
            },
            {
                test: /\.txt$/i,
                type: 'asset/source'
            },
            {
                test: /\.(csv|tsv)$/i,
                use: ['csv-loader'],
            },
            {
                test: /\.xml$/i,
                use: ['xml-loader'],
            },
            {
                test: /\.toml$/i,
                type: 'json',
                parser: {
                  parse: toml.parse,
                },
            },
            {
                test: /\.yaml$/i,
                type: 'json',
                parser: {
                  parse: yaml.parse,
                },
            },
            {
                test: /\.json5$/i,
                type: 'json',
                parser: {
                  parse: json5.parse,
                },
            }
        ]
    },
    optimization: {
        runtimeChunk:'single', 
        splitChunks: {
            cacheGroups: {              //公共Js分离
                vendor: {    
                    name: "vendor",
                    test: /[\\/]node_modules[\\/]/,
                    chunks: "all",
                    priority: 10 // 优先级
                },
                common: {
                    name: "common",
                    test: /[\\/]commonjs[\\/]/,
                    minSize: 1024,
                    chunks: "all",
                    priority: 5
                }
            }
        }
    },
};


