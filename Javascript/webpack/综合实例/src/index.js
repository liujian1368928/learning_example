import _ from 'lodash';
import './style.css';
import './style.scss';
import back from './background.jpg';
import xmlData from './data.xml';
import yamlData from './data.yaml';
import add from './commonjs/common.js';
import txt from './data.txt';

document.title='webpack 学习';
function component() {
    const element = document.createElement('div');

    // lodash（目前通过一个 script 引入）对于执行这一行是必需的
    element.innerHTML = _.join(['Hello', 'webpack'], ' ');
    return element;
}  
var el=component();  
document.body.appendChild(el);
el.innerHTML+=xmlData.note.body;
el.innerHTML+=add(50,100);

el.innerHTML+='<br/>' +  txt;

var img=new Image(250,250);
document.body.append(img);
img.src=back;
img.onclick= function(){

    import('./click.js').then(e=>{        
        e.default(2);
    });

    /*   


    import('./click.js').then(function({default:e}){
        e(3);
    });
    import('./click.js').then(function(e){
        e.default(4);
    });
    import('./click.js').then(({default:e})=>{
        e(5);
    }); 
    */
}




