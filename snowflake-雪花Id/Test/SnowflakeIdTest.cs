using Microsoft.VisualStudio.TestTools.UnitTesting;
using snowflake_ѩ��Id;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Test
{
    [TestClass]
    public class SnowflakeIdTest
    {
        [TestMethod]
        public void GenerateIdTest()
        {
            var snowflakeId = new SnowFlakeId(3, 7);
            var id = snowflakeId.GenerateId();
            Assert.AreEqual(snowflakeId.LastSnowId.DataCenterId, snowflakeId.DataCenterId);
            Assert.AreEqual(snowflakeId.LastSnowId.WorkerId, snowflakeId.WorkerId);
        }

        [TestMethod]
        public void GenerateIdsTest()
        {
            var snowflakeId = new SnowFlakeId();
            long preId = 0;
            for (var i = 0; i < 8 * 100000;i++) {
                var id = snowflakeId.GenerateId();
                Assert.AreEqual(id > preId, true);
                preId = id;
            }
        }

        [TestMethod]
        public void GenerateIdThreadTest()
        {
            var signFor = 8;
            var maxCount = 2500;
            var snowflakeId = new SnowFlakeId();
            var ids = new List<long>();
            Parallel.For(0, signFor, i =>
            {
                for (var j = 0; j < maxCount; j++)
                {
                    var id = snowflakeId.GenerateId();
                    Assert.AreEqual(id > 0, true);
                    lock (ids)
                    {
                        ids.Add(id);
                    }
                }
            });
            Assert.AreEqual(ids.Count, signFor * maxCount);

            Parallel.For(0, signFor, i =>
            {
                for (var j = 0; j < maxCount; j++)
                {
                    var id = ids[i * maxCount + j];
                    var isExsist = ids.Count(r => r == id) > 1;
                    Assert.IsFalse(isExsist);
                }
            });
        }

        [TestMethod]
        public void GenerateIdQPSTest()
        {
            var signFor = 8;
            var maxCount = 1000000;
            var snowflakeId = new SnowFlakeId();
            Stopwatch sw = new Stopwatch();
            sw.Start();
            for (var i = 0; i < signFor; i++)
            {
                for (var j = 0; j < maxCount; j++)
                {
                    var id = snowflakeId.GenerateId();
                }
            }
            sw.Stop();
            Assert.IsTrue(sw.ElapsedMilliseconds / 1000 < 3);
        }

        [TestMethod]
        public void AnalyzeIdTest()
        {
            var signFor = 8;
            var maxCount = 1000000;
            var snowflakeId = new SnowFlakeId(2, 5);
            Parallel.For(0, signFor, i =>
            {
                for (var j = 0; j < maxCount; j++)
                {
                    var id = snowflakeId.GenerateId();
                    var oldIdClass = snowflakeId.AnalyzeId(id);
                    var idClass = snowflakeId.AnalyzeId(snowflakeId.GenerateId(oldIdClass));
                    Assert.AreEqual(oldIdClass.Id, idClass.Id);
                    Assert.AreEqual(oldIdClass.Timestamp, idClass.Timestamp);
                    Assert.AreEqual(oldIdClass.Sequence, idClass.Sequence);
                    Assert.AreEqual(oldIdClass.DataCenterId, snowflakeId.DataCenterId);
                    Assert.AreEqual(oldIdClass.WorkerId, snowflakeId.WorkerId);
                    Assert.AreEqual(oldIdClass.DataCenterId, idClass.DataCenterId);
                    Assert.AreEqual(oldIdClass.WorkerId, idClass.WorkerId);
                    Assert.AreEqual(oldIdClass.UnixTimestamp, idClass.UnixTimestamp);
                    Assert.AreEqual(oldIdClass.UtcDateTime, idClass.UtcDateTime);
                }
            });
        }
    }
}