﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace snowflake_雪花Id
{
    class Program
    {
        static void Main(string[] args)
        {
            var  snowflakeId = new SnowFlakeId();
            for (var i = 0; i < 100; i++)
            {
                var idTest = snowflakeId.GenerateId();
                var snowid = snowflakeId.LastSnowId;
                Console.WriteLine($"Id:{idTest}=Time:{snowid.UtcDateTime.ToLocalTime().ToString("yyyy/MM/dd HH:mm:ss:fff")},Sequence:{snowid.Sequence},DataCenterId:{snowid.DataCenterId},WorkerId:{snowid.WorkerId}");
            } 
            var ids=new List<long>();
            for(var i = 0; i < 100; i++)
            {
                var idTest = snowflakeId.GenerateId();
                ids.Add(idTest);
            }
            foreach(var id in ids)
            {
                var snowid=snowflakeId.AnalyzeId(id);
                Console.WriteLine($"Id:{snowid.Id}=Time:{snowid.UtcDateTime.ToLocalTime().ToString("yyyy/MM/dd HH:mm:ss:fff")},Sequence:{snowid.Sequence},DataCenterId:{snowid.DataCenterId},WorkerId:{snowid.WorkerId}");
            }

        }
    }
}
