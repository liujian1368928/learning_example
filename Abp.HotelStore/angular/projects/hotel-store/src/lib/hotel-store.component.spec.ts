import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotelStoreComponent } from './hotel-store.component';

describe('HotelStoreComponent', () => {
  let component: HotelStoreComponent;
  let fixture: ComponentFixture<HotelStoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotelStoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotelStoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
