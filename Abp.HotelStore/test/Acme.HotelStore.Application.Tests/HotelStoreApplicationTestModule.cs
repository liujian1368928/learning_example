﻿using Volo.Abp.Modularity;

namespace Acme.HotelStore
{
    [DependsOn(
        typeof(HotelStoreApplicationModule),
        typeof(HotelStoreDomainTestModule)
        )]
    public class HotelStoreApplicationTestModule : AbpModule
    {

    }
}
