﻿using Acme.HotelStore.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace Acme.HotelStore
{
    /* Domain tests are configured to use the EF Core provider.
     * You can switch to MongoDB, however your domain tests should be
     * database independent anyway.
     */
    [DependsOn(
        typeof(HotelStoreEntityFrameworkCoreTestModule)
        )]
    public class HotelStoreDomainTestModule : AbpModule
    {
        
    }
}
