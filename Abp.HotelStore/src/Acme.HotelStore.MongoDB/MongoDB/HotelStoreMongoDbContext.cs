﻿using Volo.Abp.Data;
using Volo.Abp.MongoDB;

namespace Acme.HotelStore.MongoDB
{
    [ConnectionStringName(HotelStoreDbProperties.ConnectionStringName)]
    public class HotelStoreMongoDbContext : AbpMongoDbContext, IHotelStoreMongoDbContext
    {
        /* Add mongo collections here. Example:
         * public IMongoCollection<Question> Questions => Collection<Question>();
         */

        protected override void CreateModel(IMongoModelBuilder modelBuilder)
        {
            base.CreateModel(modelBuilder);

            modelBuilder.ConfigureHotelStore();
        }
    }
}