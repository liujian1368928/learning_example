﻿using JetBrains.Annotations;
using Volo.Abp.MongoDB;

namespace Acme.HotelStore.MongoDB
{
    public class HotelStoreMongoModelBuilderConfigurationOptions : AbpMongoModelBuilderConfigurationOptions
    {
        public HotelStoreMongoModelBuilderConfigurationOptions(
            [NotNull] string collectionPrefix = "")
            : base(collectionPrefix)
        {
        }
    }
}