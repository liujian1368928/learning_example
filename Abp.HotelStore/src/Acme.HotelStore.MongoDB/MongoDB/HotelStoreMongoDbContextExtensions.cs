﻿using System;
using Volo.Abp;
using Volo.Abp.MongoDB;

namespace Acme.HotelStore.MongoDB
{
    public static class HotelStoreMongoDbContextExtensions
    {
        public static void ConfigureHotelStore(
            this IMongoModelBuilder builder,
            Action<AbpMongoModelBuilderConfigurationOptions> optionsAction = null)
        {
            Check.NotNull(builder, nameof(builder));

            var options = new HotelStoreMongoModelBuilderConfigurationOptions(
                HotelStoreDbProperties.DbTablePrefix
            );

            optionsAction?.Invoke(options);
        }
    }
}