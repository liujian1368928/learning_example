﻿$(function () {
    var l = abp.localization.getResource('HotelStore');
    var editModal = new abp.ModalManager(abp.appPath + 'HotelStore/EditModal');

    editModal.onResult(function () {
        dataTable.ajax.reload();
    });

    var dataTable = $('#Table').DataTable(
        abp.libs.datatables.normalizeConfiguration({
            serverSide: true,
            paging: true,
            order: [[1, "asc"]],
            searching: false,
            scrollX: true,
            ajax: abp.libs.datatables.createAjax(acme.hotelStore.hotels.hotel.getList),
            columnDefs: [
                {
                    title: l('Actions'),
                    rowAction: {
                        items: [
                            {
                                text: l('Edit'),
                                visible: abp.auth.isGranted('HotelStore.Hotel.Edit'),
                                action: function (data) {
                                    editModal.open({ id: data.record.id });
                                }
                            }, {
                                text: l('Delete'),
                                visible: abp.auth.isGranted('HotelStore.Hotel.Delete'),
                                confirmMessage: function (data) {
                                    return l(
                                        'AreYouSureToDelete',
                                        data.record.name
                                    );
                                },
                                action: function (data) {
                                    acme.hotelStore.hotels.hotel.delete(data.record.id).done(function (result) {
                                        abp.notify.info(l('SuccessfullyDeleted'));
                                        dataTable.ajax.reload();
                                    });
                                }
                            }
                        ]
                    }
                },
                {
                    title: l('HotelName'),
                    data: "hotelName"
                },
                {
                    title: l('Address'),
                    data: "address"
                } 
            ]
        })
    );

    var createModal = new abp.ModalManager(abp.appPath + 'HotelStore/CreateModal');

    createModal.onResult(function () {
        dataTable.ajax.reload();
    });

    $('#NewBookButton').click(function (e) {
        e.preventDefault();
        createModal.open();
    });
});