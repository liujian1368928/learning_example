using Microsoft.AspNetCore.Authorization;

namespace Acme.HotelStore.Web.Pages.HotelStore
{
    [Authorize(Permissions.HotelStorePermissions.Hotel.Default)]
    public class IndexModel : HotelStorePageModel
    {
        public void OnGet()
        {
        }
    }
}