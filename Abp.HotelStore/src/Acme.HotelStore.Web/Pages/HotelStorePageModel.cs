﻿using Acme.HotelStore.Localization;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace Acme.HotelStore.Web.Pages
{
    /* Inherit your PageModel classes from this class.
     */
    public abstract class HotelStorePageModel : AbpPageModel
    {
        protected HotelStorePageModel()
        {
            LocalizationResourceType = typeof(HotelStoreResource);
            ObjectMapperContext = typeof(HotelStoreWebModule);
        }
    }
}