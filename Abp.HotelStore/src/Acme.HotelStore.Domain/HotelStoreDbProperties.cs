﻿namespace Acme.HotelStore
{
    public static class HotelStoreDbProperties
    {
        public static string DbTablePrefix { get; set; } = "HotelStore";

        public static string DbSchema { get; set; } = null;

        public const string ConnectionStringName = "HotelStore";
    }
}
