﻿using Volo.Abp.Settings;

namespace Acme.HotelStore.Settings
{
    public class HotelStoreSettingDefinitionProvider : SettingDefinitionProvider
    {
        public override void Define(ISettingDefinitionContext context)
        {
            /* Define module settings here.
             * Use names from HotelStoreSettings class.
             */
        }
    }
}