﻿using Acme.HotelStore.Localization;
using Volo.Abp.Application.Services;

namespace Acme.HotelStore
{
    public abstract class HotelStoreAppService : ApplicationService
    {
        protected HotelStoreAppService()
        {
            LocalizationResource = typeof(HotelStoreResource);
            ObjectMapperContext = typeof(HotelStoreApplicationModule);
        }
    }
}
