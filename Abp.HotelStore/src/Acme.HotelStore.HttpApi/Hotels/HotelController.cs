﻿using Acme.HotelStore.Hotels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.Application.Dtos;

namespace Acme.HotelStore.Hotels
{
    [RemoteService]
    public class HotelController : HotelStoreController, IHotelAppService
    {
        IHotelAppService hotelAppService;
        public HotelController(IHotelAppService hotelAppService)
        {
            this.hotelAppService = hotelAppService;
        }
        public async Task<HotelDto> CreateAsync(CreateOrUpdateHotelDto input)
        {
            return await hotelAppService.CreateAsync(input);
        }

        public async Task DeleteAsync(Guid id)
        {
            await hotelAppService.DeleteAsync(id);
        }

        public async Task<HotelDto> GetAsync(Guid id)
        {
            return await hotelAppService.GetAsync(id);
        }

        public async Task<PagedResultDto<HotelDto>> GetListAsync(PagedAndSortedResultRequestDto input)
        {
            return await hotelAppService.GetListAsync(input);
        }

        public async Task<HotelDto> UpdateAsync(Guid id, CreateOrUpdateHotelDto input)
        {
            return await hotelAppService.UpdateAsync(id, input);
        }
    }
}
